/* 
 * iota - a minimal library for POSIX I/O tracing
 * Copyright 2011-2013, Brown University, Providence, RI. All Rights Reserved.
 * 
 * This file is part of iota.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */


#ifndef __IOTA_UTIL_H__
#define __IOTA_UTIL_H__

#include <errno.h>

/* error handling */
#ifdef USE_MPI
void MPI_set_errno(int rank, const char* msg);
#endif

/* crazy workaround to get __LINE__ as a string */
#define STRINGIFY(x) #x
#define STRINGIFY1(x) STRINGIFY(x)
#define AT __FILE__ ":" STRINGIFY1(__LINE__)

/* Check if errno was set after a call. */
#ifdef USE_MPI
#define ERRNO_CHECK(call) \
errno = 0;\
call;\
if (errno > 0) {\
	MPI_set_errno(rank, "[libiota] " AT);\
	MPI_Abort(MPI_COMM_WORLD, errno);\
}
#else // USE_MPI
#define ERRNO_CHECK(call) \
errno = 0;\
call;\
if (errno > 0) {\
	perror("[libiota] " AT);\
	exit(errno);\
}
#endif // USE_MPI

#endif

