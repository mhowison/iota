#!/usr/bin/env python

import sys
import math
from collections import namedtuple, defaultdict
from matplotlib import pyplot as plt

LustreInfo = namedtuple('LustreInfo', "size count offset osts")

files = {}
data = defaultdict(lambda: defaultdict(list))
fildes = defaultdict(dict)
fpos = defaultdict(dict)

def parse_trace(f):
	for line in f:
		fields = line.strip().split('\t')
		if len(fields) > 5:
			rank = int(fields[0])
			op = fields[3]
		else:
			print "warning: bad line:\n%s" % line
			continue
		if 'open' in op:
			fd = int(fields[4])
			filename = fields[5]
			if len(fields) > 6:
				if rank != 0:
					print "Lustre info on rank %d:\n%s" % (rank, fiealds[6])
					sys.exit()
				l = map(int, fields[6].split(','))
				files[filename] = LustreInfo(l[0], l[1], l[2], l[3:])
				#if fd in fildes[rank]:
				#	print "warning: reused file descriptor %d on rank %d" % (fd, rank)
			fildes[rank][fd] = filename
			fpos[rank][fd] = 0
			#else:
			#	print "warning: no Lustre info for file '%s'" % fields[5]
		elif 'seek' in op:
			pos = int(fields[4])
			fd = int(fields[5])
			fpos[rank][fd] = pos
		elif op == 'write' or op == 'fwrite':
			fd = int(fields[5])
			filename = fildes[rank].get(fd)
			lustre = files.get(filename)
			if lustre:
				ost = lustre.osts[(fpos[rank][fd] / lustre.size) % lustre.count]
				bw = 100000.0 * float(fields[4]) / float(fields[2])
				data[filename][ost].append(bw)
			else:
				print "warning: no Lustre info for %s (%d)" % (filename, fd)

with open(sys.argv[1], 'r') as f:
	parse_trace(f)
	for i, filename in enumerate(data.keys()):
		print filename
		osts = files.get(filename, None).osts
		plt.figure(figsize=(max(3, 12 * len(osts) / 160.0), 3))
		plt.gca().set_xticklabels(osts, rotation=-90, size=5)
		plt.ylabel('Bandwidth', size=10)
		plt.yscale('log', basey=2)
		plt.ylim((1,2**30))
		plt.yticks([2**j for j in (0,4,7,10,14,17,20,24,27,30)])
		plt.grid(axis='y')
		labels = ['1B/s']
		for s in ('B/s', 'KB/s', 'MB/s'):
			labels += ['%d%s' % (j,s) for j in (16,128,1024)]
		plt.gca().set_yticklabels(labels, size=8)
		plt.xlabel('OST (in stripe order)', size=10)
		plt.boxplot([data[filename][ost] for ost in osts], 0, '')
		plt.tight_layout()
		plt.savefig('%s.%d.ost.pdf' % (sys.argv[1], i))

