#!/usr/bin/env python

import sys
import math
import numpy as np
from collections import namedtuple, defaultdict
import matplotlib
from matplotlib import pyplot as plt

LustreInfo = namedtuple('LustreInfo', "size count offset osts")

files = {}
#data = defaultdict(lambda: defaultdict(list))
writes = defaultdict(lambda: defaultdict(dict))
opens = defaultdict(lambda: defaultdict(dict))
closes = defaultdict(lambda: defaultdict(dict))
fildes = defaultdict(dict)
fpos = defaultdict(dict)
fstart = defaultdict(dict)

def parse_trace(f, unit):
	for line in f:
		fields = line.strip().split('\t')
		if len(fields) > 5:
			rank = int(fields[0])
			start = int(fields[1])
			elapsed = int(fields[2])
			op = fields[3]
			ret = int(fields[4])
		else:
			print "warning: bad line:\n%s" % line
			continue
		if 'open' in op:
			fd = ret
			filename = fields[5]
			if len(fields) > 6:
				if rank != 0:
					print "Lustre info on rank %d:\n%s" % (rank, fiealds[6])
					sys.exit()
				l = map(int, fields[6].split(','))
				lustre = LustreInfo(l[0], l[1], l[2], l[3:])
				files[filename] = lustre
				#if fd in fildes[rank]:
				#	print "warning: reused file descriptor %d on rank %d" % (fd, rank)
			else:
				lustre = files.get(filename)
			fildes[rank][fd] = filename
			fpos[rank][fd] = 0
			fstart[rank][fd] = int(fields[1])
			if lustre:
				rank %= len(lustre.osts)
				for i in xrange(0, elapsed / unit + 1):
					opens[filename][i][rank] = opens[filename][i].get(rank, 0) + 1
		elif 'seek' in op:
			pos = ret
			fd = int(fields[5])
			fpos[rank][fd] = pos
		elif op == 'write' or op == 'fwrite':
			fd = int(fields[5])
			filename = fildes[rank].get(fd)
			lustre = files.get(filename)
			if lustre:
				ost = (fpos[rank][fd] / lustre.size) % lustre.count
				start -= fstart[rank][fd]
				nbytes = ret / (elapsed / unit + 1)
				for i in xrange(start / unit, (start + elapsed) / unit + 1):
					writes[filename][i][ost] = writes[filename][i].get(ost, 0) + nbytes
			else:
				print "warning: no Lustre info for %s (%d)" % (filename, fd)
		elif 'close' in op:
			fd = int(fields[5])
			filename = fildes[rank].get(fd)
			lustre = files.get(filename)
			if lustre:
				start -= fstart[rank][fd]
				rank %= len(lustre.osts)
				for i in xrange(start / unit, (start + elapsed) / unit + 1):
					closes[filename][i][rank] = closes[filename][i].get(rank, 0) + 1

if len(sys.argv) > 2:
	unit = int(sys.argv[2])
else:
	unit = 100000

def plot(data, c):
	for x in data:
		norm = 1.0 / float(sum(data[x].itervalues()))
		for y,a in data[x].iteritems():
			a *= norm
			plt.barh(y - 0.5, 1, height=1, left=x, color=c, ec='none', alpha=a)
			plt.hlines(y, x, x+1, color=c)

with open(sys.argv[1], 'r') as f:
	parse_trace(f, unit)
	for i, filename in enumerate(writes.keys()):
		print filename
		osts = files.get(filename, None).osts
		n = len(osts)
		plt.figure(figsize=(12, 12), dpi=150)
		plt.gca().set_yticklabels(osts, size=5)
		plt.ylabel("OST (in stripe order)", size=10)
		plt.ylim(-0.5, len(osts) - 0.5)
		plt.yticks(np.arange(len(osts)))
		plt.grid(axis='y')
		plt.xlabel("Runtime (from file open) in %gms increments" % (0.001*unit), size=10)
		plt.gca().tick_params(axis='x', direction='out')
		plt.gca().tick_params(axis='y', direction='out')
		plot(closes[filename], 'r')
		plot(opens[filename], 'g')
		#plot(writes[filename], 'b')
		plt.tight_layout()
		plt.savefig('%s.%d.pix.png' % (sys.argv[1], i))

