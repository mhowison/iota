/* 
 * iota - a minimal library for POSIX I/O tracing
 * Copyright 2011-2013, Brown University, Providence, RI. All Rights Reserved.
 * 
 * This file is part of iota.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */


#include <stdio.h>
#include <stdlib.h>

#define MAX_FD 1000

FILE* file;
int numparticles = 1000;
int x_dim = 64;
int y_dim = 64; 
int z_dim = 64;

double uniform_random_number() {
	return (((double)rand())/((double)(RAND_MAX)));
}

void open_test() {
	FILE* fds[MAX_FD];
	int i;
	for (i=0; i<MAX_FD; i++) {
		char name[64];
		sprintf(name, "/tmp/iota-test-fd-%d", i);
		fds[i] = fopen(name, "w");
	}
	for (i=0; i<MAX_FD; i++) {
		fclose(fds[i]);
	}
}

void create_synthetic_data() {
	double *x, *y, *z;
	x=(double*)malloc(numparticles*sizeof(double));
	y=(double*)malloc(numparticles*sizeof(double));
	z=(double*)malloc(numparticles*sizeof(double));
	
	int *id;
	id=(int*)malloc(numparticles*sizeof(int));

	int i;
	for (i=0; i<numparticles; i++) {
		id[i] = i;
		x[i] = uniform_random_number()*x_dim;
		y[i] = uniform_random_number()*y_dim;
		z[i] = ((double)i/numparticles)*z_dim;		
	}

	fwrite(id, sizeof(int), numparticles, file);
	fwrite(x, sizeof(double), numparticles, file);
	fwrite(y, sizeof(double), numparticles, file);
	fwrite(z, sizeof(double), numparticles, file);
	
	free(x); free(y); free(z);
	free(id);
}

int main(int argc, char* argv[]) {
	
	file = fopen("sample.dat", "w");

	create_synthetic_data();

	fclose(file);

	open_test();

	return 0;
}
