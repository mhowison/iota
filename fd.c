/* 
 * iota - a minimal library for POSIX I/O tracing
 * Copyright 2011-2013, Brown University, Providence, RI. All Rights Reserved.
 * 
 * This file is part of iota.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

#define NBITS 8

static char* fd_bits;
static size_t fd_bits_size;
static int fd_max;

static void fd_dump_bits(int fd, int d, int r, char mask)
{
	char b[2*NBITS+2];
	b[NBITS] = ' ';
	b[2*NBITS+1] = '\0';
	int j, k;
	for (j=0; j<NBITS; j++) {
		k = 0x01 << j;
		b[j] = (mask & k) ? '1' : '0';
	}
	for (j=0; j<NBITS; j++) {
		k = 0x01 << j;
		b[NBITS+j+1] = (fd_bits[d] & k) ? '1' : '0';
	}
	printf("%d %zd %d %d %d %s\n", fd_max, fd_bits_size, fd, d, r, b);
}

static void fd_realloc(int n)
{
	fd_max = fd_max + fd_max/2;
	if (n > fd_max) fd_max = n;
	size_t new_size = fd_max / NBITS + 1;
	ERRNO_CHECK(fd_bits = realloc(fd_bits, new_size))
	memset(fd_bits + fd_bits_size, 0x00, new_size - fd_bits_size);
	fd_bits_size = new_size;
}

void iota_fd_init(int n)
{
	fd_max = n;
	fd_bits_size = n / NBITS + 1;
	ERRNO_CHECK(fd_bits = malloc(fd_bits_size))
	memset(fd_bits, 0x00, fd_bits_size);
}

void iota_fd_fini()
{
	free(fd_bits);
}

int iota_fd_check(int fd)
{
	if (fd <= 0 || fd > fd_max) return 0;

	int d = fd / NBITS;
	int r = fd % NBITS;
	int mask = (0x01 << r);

#if DEBUG
	//fd_dump_bits(fd, d, r, mask);
#endif

	return fd_bits[d] & mask;
}

void iota_fd_toggle(int fd)
{
	if (fd <= 0) return;
	else if (fd > fd_max) fd_realloc(fd);

	int d = fd / NBITS;
	int r = fd % NBITS;
	int mask = (0x01 << r);

#if DEBUG
	fd_dump_bits(fd, d, r, mask);
#endif

	fd_bits[d] ^= mask;
}

