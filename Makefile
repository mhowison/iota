CC = gcc
MPICC = mpicc

CFLAGS = -fPIC -D_LARGEFILE64_SOURCE -DIOTA_VERSION=1
LDFLAGS = -fPIC
LIBS =

TARGETS = libiota.a libiota-mpi.a wraps.txt test

ifdef LUSTRE
CFLAGS += -DHAVE_LUSTRE -I$(LUSTRE)/include
LUSTRELIB = -llustreapi
endif

ifdef H5PART
CFLAGS += -I$(H5PART)/include -DPARALLEL_IO
LDFLAGS += -L$(H5PART)/lib
LIBS += -lH5Part -lhdf5 -lz
TARGETS += test-mpi
endif

ifdef SHARED
TARGETS += libiota.so libiota-mpi.so
endif

all:	$(TARGETS)

DEP = fd.o util.h dlsym.h wrap.h

libiota.a:	iota.o $(DEP)
	ar rcs $@ $(filter %.o, $^)

libiota.so:	iota.c $(DEP)
	$(CC) $(CFLAGS) -DUSE_DLSYM -o iota-dlsym.o -c $<
	$(CC) $(LDFALGS) -shared -o $@ iota-dlsym.o $(filter %.o, $^) $(LIBS) -ldl

libiota-mpi.a:	iota.c $(DEP) mpierrno.o
	$(MPICC) $(CFLAGS) -DUSE_MPI -o iota-mpi.o -c $<
	ar rcs $@ iota-mpi.o $(filter %.o, $^)

libiota-mpi.so:	iota.c $(DEP) mpierrno.o
	$(MPICC) $(CFLAGS) -DUSE_MPI -DUSE_DLSYM -o iota-mpi-dlsym.o -c $<
	$(MPICC) $(LDFALGS) -shared -o $@ iota-mpi-dlsym.o $(filter %.o, $^) $(LIBS) -ldl

wraps.txt:	libiota.a
	./gen_wrap.sh $< >$@

test:	test.o
	$(CC) $(LDFLAGS) $(shell cat wraps.txt) -o $@ $< $(LIBS) libiota.a $(LUSTRELIB)

test-mpi:	sample.h5part.c libiota-mpi.a
	$(MPICC) $(CFLAGS) -o sample.h5part.o -c sample.h5part.c
	$(MPICC) $(LDFLAGS) $(shell cat wraps.txt) -o $@ sample.h5part.o $(LIBS) libiota-mpi.a $(LUSTRELIB)

clean:
	-rm libiota.a libiota-mpi.a wraps.txt test test-mpi *.o

