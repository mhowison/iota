/* 
 * iota - a minimal library for POSIX I/O tracing
 * Copyright 2011-2013, Brown University, Providence, RI. All Rights Reserved.
 * 
 * This file is part of iota.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <limits.h>
#include <fnmatch.h>

#ifdef USE_DLSYM
#include "dlsym.h"
#else
#include "wrap.h"
#endif

#ifdef USE_MPI
#include <mpi.h>
#endif

#ifdef HAVE_LUSTRE
#include <lustre/liblustreapi.h>
#endif

#include "util.h"
#include "fd.h"

/* Function pointer type provides support for multiple recorder functions, with
 * one chosen at runtime. */
typedef void (*buffer_op_t)();

/* globals */

static int recording = 0;

#define RECORD_PREFIX if (recording && buffer) {
#define RECORD_SUFFIX } \
	iota_usec += get_current_usec() - end_usec;

#ifdef HAVE_LUSTRE
#define MAX_OSTS 1024
#define LOV_EA_SIZE(lum, num) (sizeof(*lum) + num * sizeof(*lum->lmm_objects))
#define LOV_EA_MAX(lum) LOV_EA_SIZE(lum, MAX_OSTS)
static struct lov_user_md* lum;
#endif

static long init_usec = 0;
static long iota_usec = 0;

static int rank = -1;

static char logname[64]; /* leaves room for an %ld (21) and a %d (11) */
static FILE* logfile = NULL;

static char* path_pattern = NULL;

static char* buffer = NULL;
static size_t bufpos = 0;
/* bufpad needs to be large enough to accomodate an entire line of output:
   * the common fields
     task start elapse op ret
   * additional fields path, lustre-string or fd
   Also, bufpad should be a multiple of 1MB so that when subtracted from
   bufsize, it leads to a 1MB aligned write in record-per-writer mode.
*/
static size_t bufpad = 1024*1024;
static size_t bufsize = 2*1024*1024;
static size_t bufmax = 1024*1024;

static void extend_buffer();
static buffer_op_t buffer_op = extend_buffer;

/* Control function for enabling I/O tracing only for specific code regions. */
void iota_start_recording() { recording = 1; }
void iota_stop_recording() { recording = 0; }

/* Core timer function tries to use the highest precision clock available. */
static long get_current_usec()
{
#if HAVE_GETTIME
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (1000000L * (long)ts.tv_sec) + (ts.tv_nsec / 1000L);
#else
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (1000000L * (long)tv.tv_sec) + (long)tv.tv_usec;
#endif
}

#define TIMER_START size_t start_usec = get_current_usec();
#define TIMER_END size_t end_usec = get_current_usec();

static int check_path(const char* path)
{
	recording = 0;
	int match = 0;
	if (path_pattern != NULL) {
		if (fnmatch(path_pattern, path, 0) == 0) match = 1;
#if DEBUG
		fprintf(stderr, "[libiota] match %s against %s = %d\n",
						path_pattern, path, match);
#endif
	} else {
		match = 1;
#if DEBUG
		fprintf(stderr, "[libiota] no path_pattern, keeping %s\n", path);
#endif
	}
	recording = 1;
	return match;
}

static void check_close(int fildes)
{
	recording = 0;
	if (iota_fd_check(fildes)) iota_fd_toggle(fildes);
	recording = 1;
}

/* buffering functions */

/* For single-file recording, buffer needs to be extended. */
static void extend_buffer()
{
	recording = 0;
	bufsize += bufsize / 2;
	bufmax = bufsize - bufpad;
	ERRNO_CHECK(buffer = (char*)realloc(buffer, bufsize))
	buffer = (char*)realloc(buffer, bufsize);
	recording = 1;
}

static void open_logfile(const char* path)
{
#ifdef HAVE_LUSTRE
	ERRNO_CHECK(int fd = llapi_file_create(path, 1024*1024, -1, 1, LOV_PATTERN_RAID0))
	ERRNO_CHECK(close(fd))
#endif
	ERRNO_CHECK(logfile = fopen(logname, "w"))
}

/* For per-writer recording, buffer needs to be flushed to logfile. */
static void flush_buffer()
{
	recording = 0;
	/* global: logname, init_usec, rank */
	if (logfile == NULL) {
		sprintf(logname, "iota.%ld.%d.txt", init_usec, rank);
		open_logfile(logname);
	}
	ERRNO_CHECK(fwrite(buffer, sizeof(char), bufmax, logfile))
	ERRNO_CHECK(fflush(logfile))
	memmove(buffer, buffer + bufmax, bufpos - bufmax);
	bufpos -= bufmax;
	recording = 1;
}

/* recorder function */

static void record(
		const char* op,
		long start_usec,
		long end_usec,
		off_t offset,
		int fildes,
		int64_t retval)
{
	/* global: buffer, bufpos, bufsize, min_bufsize */
	if (iota_fd_check(fildes)) {
		if (bufpos > bufmax) buffer_op();
		bufpos += sprintf(buffer + bufpos,
				"%d\t%ld\t%ld\t%ld\t%s\t%zd\t%d\n",
				rank,
				start_usec - init_usec,
				end_usec - start_usec,
				offset,
				op,
				retval,
				fildes);
	}
}

static void record_path(
		const char* op,
		long start_usec,
		long end_usec,
		int retval,
		const char* path)
{
	/* global: buffer, bufpos, bufsize, min_bufsize */
	if (check_path(path)) {
		if (bufpos > bufmax) buffer_op();
		bufpos += sprintf(buffer + bufpos,
				"%d\t%ld\t%ld\t0\t%s\t%d\t%s\n",
				rank,
				start_usec - init_usec,
				end_usec - start_usec,
				op,
				retval,
				path);
	}
}

static void record_open(
		const char* op,
		long start_usec,
		long end_usec,
		int retval,
		const char* path)
{
	/* global: buffer, bufpos, bufsize, min_bufsize */
	if (check_path(path)) {
		iota_fd_toggle(retval);
		if (bufpos > bufmax) buffer_op();
		bufpos += sprintf(buffer + bufpos,
				"%d\t%ld\t%ld\t0\t%s\t%d\t%s",
				rank,
				start_usec - init_usec,
				end_usec - start_usec,
				op,
				retval,
				path);

#ifdef HAVE_LUSTRE
		if (rank == 0) {
			recording = 0;
			errno = 0;
			llapi_file_get_stripe(path, lum);
			if (errno != 0 && rank == 0) {
				fprintf(stderr,
					"[libiota] " AT " %s\n"
					"[libiota] #0 llapi_file_get_stripe(%s, %p)\n"
					"[libiota] #1 record_lustre(%s, %p)\n",
					strerror(errno), path, lum, path, lum);
			} else {
				bufpos += sprintf(buffer + bufpos, "\t%u,%u,%u",
					(unsigned)lum->lmm_stripe_size,
					(unsigned)lum->lmm_stripe_count,
					(unsigned)lum->lmm_stripe_offset);

				unsigned i;
				for (i=0; i<lum->lmm_stripe_count; i++) {
					bufpos += sprintf(buffer + bufpos, ",%u",
						(unsigned)lum->lmm_objects[i].l_ost_idx);
				}
			}
			recording = 1;
		}
#endif // HAVE_LUSTRE
		buffer[bufpos++] = '\n';
	}
}

/* wrapped POSIX I/O functions */

WRAP_FUNC3(ssize_t, write, int fildes, const void *buf, size_t nbyte)
{
        off_t offset;
	offset = lseek(fildes, 0, SEEK_CUR);
	TIMER_START
	DLSYM(write)
	ssize_t ret = __real_write(fildes, buf, nbyte);
	TIMER_END
	RECORD_PREFIX
	record("write", start_usec, end_usec, offset, fildes, ret);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC4(size_t, fwrite, const void *ptr, size_t size, size_t nitems, FILE *stream)
{
	long int offset;
	offset = ftell(stream);
	TIMER_START
	DLSYM(fwrite)
	size_t ret = __real_fwrite(ptr, size, nitems, stream);
	TIMER_END
	RECORD_PREFIX
	record("fwrite", start_usec, end_usec, offset, fileno(stream), ret*size);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC4(ssize_t, pwrite, int fildes, const void *buf, size_t nbyte, off_t offset)
{
	TIMER_START
	DLSYM(pwrite)
	ssize_t ret = __real_pwrite(fildes, buf, nbyte, offset);
	TIMER_END
	RECORD_PREFIX
	record("pwrite", start_usec, end_usec, offset, fildes, ret);
	RECORD_SUFFIX
	return ret;
}
#ifdef _LARGEFILE64_SOURCE
WRAP_FUNC4(ssize_t, pwrite64, int fildes, const void *buf, size_t nbyte, off_t offset)
{
	TIMER_START
	DLSYM(pwrite64)
	ssize_t ret = __real_pwrite64(fildes, buf, nbyte, offset);
	TIMER_END
	RECORD_PREFIX
	record("pwrite64", start_usec, end_usec, offset, fildes, ret);
	RECORD_SUFFIX
	return ret;
}
#endif

WRAP_FUNC3(ssize_t, read, int fildes, void *buf, size_t nbyte)
{
        off_t offset;
	offset = lseek(fildes, 0, SEEK_CUR);
	TIMER_START
	DLSYM(read)
	ssize_t ret = __real_read(fildes, buf, nbyte);
	TIMER_END
	RECORD_PREFIX
	record("read", start_usec, end_usec, offset, fildes, ret);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC4(size_t, fread, void *__restrict__ ptr, size_t size, size_t nitems, FILE *__restrict__ stream)
{
	long int offset;
	offset = ftell(stream);
	TIMER_START
	DLSYM(fread)
	size_t ret = __real_fread(ptr, size, nitems, stream);
	TIMER_END
	RECORD_PREFIX
	record("fread", start_usec, end_usec, offset, fileno(stream), ret*size);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC4(ssize_t, pread, int fildes, const void *buf, size_t nbyte, off_t offset)
{
	TIMER_START
	DLSYM(pread)
	ssize_t ret = __real_pread(fildes, buf, nbyte, offset);
	TIMER_END
	RECORD_PREFIX
	record("pread", start_usec, end_usec, offset, fildes, ret);
	RECORD_SUFFIX
	return ret;
}

#ifdef _LARGEFILE64_SOURCE
WRAP_FUNC4(ssize_t, pread64, int fildes, const void *buf, size_t nbyte, off_t offset)
{
	TIMER_START
	DLSYM(pread64)
	ssize_t ret = __real_pread64(fildes, buf, nbyte, offset);
	TIMER_END
	RECORD_PREFIX
	record("pread64", start_usec, end_usec, offset, fildes, ret);
	RECORD_SUFFIX
	return ret;
}
#endif

WRAP_FUNC3(off_t, lseek, int fildes, off_t offset, int whence)
{
	TIMER_START
	DLSYM(lseek)
	off_t ret = __real_lseek(fildes, offset, whence);
	TIMER_END
	RECORD_PREFIX
	record("lseek", start_usec, end_usec, offset, fildes, ret);
	RECORD_SUFFIX
	return ret;
}

#ifdef _LARGEFILE64_SOURCE
WRAP_FUNC3(off64_t, lseek64, int fildes, off64_t offset, int whence)
{
	TIMER_START
	DLSYM(lseek64)
	off64_t ret = __real_lseek64(fildes, offset, whence);
	TIMER_END
	RECORD_PREFIX
	record("lseek64", start_usec, end_usec, offset, fildes, ret);
	RECORD_SUFFIX
	return ret;
}
#endif

WRAP_FUNC3(int, fseek, FILE *stream, long offset, int whence)
{
	TIMER_START
	DLSYM(fseek)
	int ret = __real_fseek(stream, offset, whence);
	TIMER_END
	RECORD_PREFIX
	record("fseek", start_usec, end_usec, offset, fileno(stream), offset);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC2(int, truncate, const char *path, off_t length)
{
	TIMER_START
	DLSYM(truncate)
	int ret = __real_truncate(path, length);
	TIMER_END
	RECORD_PREFIX
	record_path("truncate", start_usec, end_usec, ret, path);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC2(int, ftruncate, int fildes, off_t length)
{
	TIMER_START
	DLSYM(ftruncate)
	int ret = __real_ftruncate(fildes, length);
	TIMER_END
	RECORD_PREFIX
	/* not clear what a reasonable 'offset' for this operation would be.
	 * they have no impact on the file offset */
	record("ftruncate", start_usec, end_usec, 0, fildes, length);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC1(int, fflush, FILE *stream)
{
	TIMER_START
	DLSYM(fflush)
	int ret = __real_fflush(stream);
	TIMER_END
	RECORD_PREFIX
	record("fflush", start_usec, end_usec, 0, fileno(stream), ret);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC1(int, fsync, int fildes)
{
	TIMER_START
	DLSYM(fsync)
	int ret = __real_fsync(fildes);
	TIMER_END
	RECORD_PREFIX
	record("fsync", start_usec, end_usec, 0, fildes, ret);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC2(FILE*, fopen, const char *__restrict__ filename, const char *__restrict__ mode)
{
	TIMER_START
	DLSYM(fopen)
	FILE* ret = __real_fopen(filename, mode);
	TIMER_END
	RECORD_PREFIX
	record_open("fopen", start_usec, end_usec, fileno(ret), filename);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC3(int, open, const char *path, int oflag, mode_t mode)
{
	TIMER_START
	DLSYM(open)
	int ret = __real_open(path, oflag, mode);
	TIMER_END
	RECORD_PREFIX
	record_open("open", start_usec, end_usec, ret, path);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC2(int, creat, const char *path, mode_t mode)
{
	TIMER_START
	DLSYM(creat)
	int ret = __real_creat(path, mode);
	TIMER_END
	RECORD_PREFIX
	record_open("creat", start_usec, end_usec, ret, path);
	RECORD_SUFFIX
	return ret;
}

#ifdef _LARGEFILE64_SOURCE
WRAP_FUNC3(int, open64, const char *path, int oflag, mode_t mode)
{
	TIMER_START
	DLSYM(open64)
	int ret = __real_open64(path, oflag, mode);
	TIMER_END
	RECORD_PREFIX
	record_open("open64", start_usec, end_usec, ret, path);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC2(int, creat64, const char *path, mode_t mode)
{
	TIMER_START
	DLSYM(creat64)
	int ret = __real_creat64(path, mode);
	TIMER_END
	RECORD_PREFIX
	record_open("creat64", start_usec, end_usec, ret, path);
	RECORD_SUFFIX
	return ret;
}
#endif

WRAP_FUNC2(FILE*, fdopen, int fildes, const char *mode)
{
	TIMER_START
	DLSYM(fdopen)
	FILE* ret = __real_fdopen(fildes, mode);
	TIMER_END
	RECORD_PREFIX
	/* could maybe parse mode and record offset accordingly? */
	record("fdopen", start_usec, end_usec, 0, fildes, fileno(ret));
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC3(FILE*, freopen, const char *__restrict__ filename, const char *__restrict__ mode, FILE *__restrict__ stream)
{
	TIMER_START
	DLSYM(freopen)
	FILE* ret = __real_freopen(filename, mode, stream);
	TIMER_END
	RECORD_PREFIX
	record_open("freopen", start_usec, end_usec, fileno(ret), filename);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC1(int, fclose, FILE *stream)
{
	TIMER_START
	int fildes = fileno(stream);
	DLSYM(fclose)
	int ret = __real_fclose(stream);
	TIMER_END
	RECORD_PREFIX
	record("fclose", start_usec, end_usec, 0, fildes, ret);
	check_close(fildes);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC1(int, close, int fildes)
{
	TIMER_START
	DLSYM(close)
	int ret = __real_close(fildes);
	TIMER_END
	RECORD_PREFIX
	record("close", start_usec, end_usec, 0, fildes, ret);
	check_close(fildes);
	RECORD_SUFFIX
	return ret;
}

WRAP_FUNC1(int, unlink, const char *path)
{
	TIMER_START
	DLSYM(unlink)
	int ret = __real_unlink(path);
	TIMER_END
	RECORD_PREFIX
	record_path("unlink", start_usec, end_usec, ret, path);
	RECORD_SUFFIX
	return ret;
}

/* wrapped MPI-IO functions */

#if USE_MPIIO

int MPI_File_open(MPI_Comm comm, char *filename, int amode, MPI_Info info, MPI_File *fh)
{
	TIMER_START
	int ret = PMPI_File_open(comm, filename, amode, info, fh);
	TIMER_END
	RECORD_PREFIX
	record_open("mopen", start_usec, end_usec, (long)(*fh), (long)ret, filename);
	RECORD_SUFFIX
	return ret;
}

int MPI_File_close(MPI_File *fh)
{
	TIMER_START
	int ret = PMPI_File_close(fh);
	TIMER_END
	RECORD_PREFIX
	record("mclose", "", start_usec, end_usec, 0, (long)(*fh), (long)ret);
	RECORD_SUFFIX
	return ret;
}

int MPI_File_delete(char *filename, MPI_Info info)
{
	TIMER_START
	int ret = PMPI_File_delete(filename, info);
	TIMER_END
	RECORD_PREFIX
	record_path("mdelete", start_usec, end_usec, (long)ret, filename);
	RECORD_SUFFIX
	return ret;
}

int MPI_File_set_size(MPI_File fh, MPI_Offset size)
{
	TIMER_START
	int ret = PMPI_File_set_size(fh, size);
	TIMER_END
	RECORD_PREFIX
	record("msetsize", start_usec, end_usec, 0, (long)fh, (long)ret);
	RECORD_SUFFIX
	return ret;
}

int MPI_File_preallocate(MPI_File fh, MPI_Offset size) 
{
	TIMER_START
	int ret = PMPI_File_preallocate(fh, size);
	TIMER_END
	RECORD_PREFIX
	record("mprealloc", start_usec, end_usec, 0, (long)fh, (long)ret);
	RECORD_SUFFIX
	return ret;
}

int MPI_File_get_size(MPI_File fh, MPI_Offset *size)
{
	TIMER_START
	int ret = PMPI_File_get_size(fh, size);
	TIMER_END
	RECORD_PREFIX
	record("mgetsize", "", start_usec, end_usec, 0, (long)fh, (long)ret);
	RECORD_SUFFIX
	return ret;
}

int MPI_File_set_view(MPI_File fh, MPI_Offset disp, MPI_Datatype etype, MPI_Datatype filetype, char *datarep, MPI_Info info)
{
	TIMER_START
	int ret = PMPI_File_set_view(fh, disp, etype, filetype, datarep, info);
	TIMER_END
	RECORD_PREFIX
	record("msetview", start_usec, end_usec, 0, (long)fh, (long)ret);
	RECORD_SUFFIX
	return ret;
}

int MPI_File_read_at(MPI_File fh, MPI_Offset offset, void *buf, int count, MPI_Datatype datatype, MPI_Status *status)
{
	TIMER_START
	int ret = PMPI_File_read_at(fh, offset, buf, count, datatype, status);
	TIMER_END
	RECORD_PREFIX
	record("mreadat", start_usec, end_usec, offset, (long)fh, (long)ret);
	RECORD_SUFFIX
	return ret;
}

#endif // USE_MPIIO

/* initialization and MPI hooks */

#ifdef USE_MPI
static
#else
__attribute__((constructor))
#endif
void iota_init()
{
	/* global: init_usec, buffer, record */
	init_usec = get_current_usec();

	/* setup global variables */
#ifdef USE_MPI
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#else
	/* TODO: use rank to store thread ID */
	rank = 0;
#endif
	if (rank == 0) {
		fprintf(stderr, "[libiota] initializing (version %d)\n", IOTA_VERSION);
	}

	/* get options from env. variable */
	char * token;
	char* env = getenv("LIBIOTA");
	/* Bluegene strtok chokes on null */
	if (env == NULL)
	    token = NULL;
	else
	    token = strtok(env, ":");
	while (token != NULL) {
		if (strncmp(token, "PER_WRITER", 10) == 0) {
			buffer_op = flush_buffer;
		} else if (strncmp(token, "PATHS=", 6) == 0) {
			path_pattern = token + 6;
		} else if (strncmp(token, "BUFFER=", 7) == 0) {
			int i = atoi(token + 7);
			if (i < 2*bufpad) i = 2*bufpad;
			bufsize = (size_t)i;
		}
		token = strtok(NULL, ":");
	}

	ERRNO_CHECK(buffer = (char*)malloc(bufsize))

#ifdef HAVE_LUSTRE
	ERRNO_CHECK(lum = (struct lov_user_md*)malloc(LOV_EA_MAX(lum)))
#endif

	/* start with space for 127 file descriptors */
	iota_fd_init(127);

	iota_start_recording();

	iota_usec += get_current_usec() - init_usec;
}

#ifdef USE_MPI
int MPI_Init(int* argc, char*** argv)
{
	int ret = PMPI_Init(argc, argv);
	iota_init();
	return ret;
}

int MPI_Init_thread(int* argc, char*** argv, int required, int* provided)
{
	int ret = PMPI_Init_thread(argc, argv, required, provided);
	iota_init();
	return ret;
}
#endif // USE_MPI

/* finalize */

/* For per-writer recording, buffer needs to be gathered at finalize. */
static void gather_buffer()
{
#ifdef USE_MPI
	int ntasks;
	int ibufpos = (int)bufpos;
	int nbytes;
	int* counts;
	int* offsets;
	char* buffer_all;

	MPI_Comm_size(MPI_COMM_WORLD, &ntasks);

	if (rank == 0) {
		ERRNO_CHECK(counts = malloc(ntasks*sizeof(int)))
		ERRNO_CHECK(offsets = malloc(ntasks*sizeof(int)))
	}

	size_t gather_start = get_current_usec();

	MPI_Gather(&ibufpos, 1, MPI_INT, counts, 1, MPI_INT, 0, MPI_COMM_WORLD);

	if (rank == 0) {
		int i;
		nbytes = 0;
		for (i=0; i<ntasks; i++) nbytes += counts[i];
		ERRNO_CHECK(buffer_all = (char*)malloc(nbytes))
		offsets[0] = 0;
		for (i=1; i<ntasks; i++) offsets[i] = offsets[i-1] + counts[i-1];
	}

	MPI_Gatherv(buffer, ibufpos, MPI_CHAR, buffer_all, counts, offsets, MPI_CHAR, 0, MPI_COMM_WORLD);

	size_t gather_usec = get_current_usec() - gather_start;
	if (rank == 0) {
		fprintf(stderr, "[libiota] gather time = %zu usec\n", gather_usec);
		free(counts);
		free(offsets);
	}

#endif // USE_MPI

	if (rank == 0) {
		/* global: logname, init_usec */
		sprintf(logname, "iota.%ld.txt", init_usec);
		open_logfile(logname);
#ifdef USE_MPI
		ERRNO_CHECK(fwrite(buffer_all, 1, nbytes, logfile))
		free(buffer_all);
#else
		ERRNO_CHECK(fwrite(buffer, 1, bufpos, logfile))
#endif
		ERRNO_CHECK(fclose(logfile))
	}
}

static void flush_buffer_final()
{
	/* global: bufpos, logname, init_usec, rank */
	if (bufpos > 0) {
		if (logfile == NULL) {
			sprintf(logname, "iota.%ld.%d.txt", init_usec, rank);
			open_logfile(logname);
		}
		ERRNO_CHECK(fwrite(buffer, sizeof(char), bufpos, logfile))
		ERRNO_CHECK(fclose(logfile))
	}
}

#if USE_MPI
int MPI_Finalize()
#else
__attribute__ ((destructor))
void iota_fini()
#endif
{
	size_t start_usec = get_current_usec();

	iota_fd_fini();
	iota_stop_recording();

	if (buffer_op == extend_buffer) {
		gather_buffer();
	} else {
		flush_buffer_final();
	}

	free(buffer);
#ifdef HAVE_LUSTRE
	free(lum);
#endif

	size_t end_usec = get_current_usec();
	iota_usec += end_usec - start_usec;
	size_t total_usec = end_usec - init_usec;

	if (rank == 0) {
		fprintf(stderr,
			"[libiota] final buffer size = %zu bytes\n", bufsize);
	}

#ifdef USE_MPI
	size_t usec;

	MPI_Reduce(&iota_usec, &usec, 1, MPI_LONG, MPI_MAX, 0, MPI_COMM_WORLD);

	if (rank == 0) {
		fprintf(stderr, "[libiota] max overhead = %ld usec\n", usec);
	}

	MPI_Reduce(&total_usec, &usec, 1, MPI_LONG, MPI_MAX, 0, MPI_COMM_WORLD);

	if (rank == 0) {
		fprintf(stderr, "[libiota] max walltime = %ld usec\n", usec);
	}

	int ret = PMPI_Finalize();
	return ret;
#else
	if (rank == 0) {
		fprintf(stderr, "[libiota] overhead = %ld usec\n", iota_usec);
		fprintf(stderr, "[libiota] walltime = %ld usec\n", total_usec);
	}
#endif // USE_MPI
}

